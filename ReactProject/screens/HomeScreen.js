import React, {Component} from 'react';
import { StyleSheet, Text, View, Button, ScrollView } from 'react-native';

class HomeScreen extends Component {
    render() {
        return (
          <View style={styles.container}>
              <ScrollView>
              <Text style={styles.title}>React-Native Components</Text>
                    <Button color="#df942a"
                title="Details"
                onPress={() => this.props.navigation.navigate('Details')}
              />
              <Text>  </Text>
              <Button color="#df942a"
                title="Activity Indicator"
                onPress={() => this.props.navigation.navigate('ActivityIndicator')}
              />
               <Text>  </Text>
              <Button color="#df942a"
                title="Drawer Layout"
                onPress={() => this.props.navigation.navigate('DrawerLayout')}
              />
               <Text>  </Text>
              <Button color="#df942a"
                title="Image Screen"
                onPress={() => this.props.navigation.navigate('ImageScreen')}
              />
               <Text>  </Text>
              <Button color="#df942a"
                title="Keyboard Avoid Screen"
                onPress={() => this.props.navigation.navigate('KeyboardAvoid')}
              />
               <Text>  </Text>
              <Button color="#df942a"
                title="List View"
                onPress={() => this.props.navigation.navigate('ListView')}
              />
               <Text>  </Text>
              <Button color="#df942a"
                title="Modal"
                onPress={() => this.props.navigation.navigate('ModalScreen')}
              />
               <Text>  </Text>
              <Button color="#df942a"
                title="Picker Screen"
                onPress={() => this.props.navigation.navigate('PickerScreen')}
              />
               <Text>  </Text>
              <Button color="#df942a"
                  title="Progress Bar"
                  onPress={() => this.props.navigation.navigate('ProgressBar')}
              />
               <Text>  </Text>
              <Button color="#df942a"
                  title="Refresh Control"
                  onPress={() => this.props.navigation.navigate('RefreshControl')}
              />
               <Text>  </Text>
              <Button color="#df942a"
                  title="Scroll View"
                  onPress={() => this.props.navigation.navigate('ScrollView')}
              />
               <Text>  </Text>
              <Button color="#df942a"
                  title="Section List"
                  onPress={() => this.props.navigation.navigate('SectionList')}
              />
               <Text>  </Text>
              <Button color="#df942a"
                  title="Slider"
                  onPress={() => this.props.navigation.navigate('Slider')}
              />
               <Text>  </Text>
              <Button color="#df942a"
                  title="Status Bar"
                  onPress={() => this.props.navigation.navigate('StatusBar')}
              />
               <Text>  </Text>
              <Button color="#df942a"
                  title="Switch Screen"
                  onPress={() => this.props.navigation.navigate('Switch')}
              />
               <Text>  </Text>
              <Button color="#df942a"
                  title="Text Input"
                  onPress={() => this.props.navigation.navigate('TextInput')}
              />
               <Text>  </Text>
              <Button color="#df942a"
                  title="Text Screen"
                  onPress={() => this.props.navigation.navigate('Text')}
              />
               <Text>  </Text>
              <Button color="#df942a"
                  title="Touchable HighLight"
                  onPress={() => this.props.navigation.navigate('TouchableHighlight')}
              />
               <Text>  </Text>
              <Button color="#df942a"
                  title="Touchable Native Feedback"
                  onPress={() => this.props.navigation.navigate('TouchableNativeFeedback')}
              />
               <Text>  </Text>
              <Button color="#df942a"
                  title="Touchable Opacity"
                  onPress={() => this.props.navigation.navigate('TouchableOpacity')}
              />
               <Text>  </Text>
              <Button color="#df942a"
                  title="View Pager Android"
                  onPress={() => this.props.navigation.navigate('ViewPager')}
              />
               <Text>  </Text>
              <Button color="#df942a"
                  title="View Screen"
                  onPress={() => this.props.navigation.navigate('View')}
              />
               <Text>  </Text>
              <Button color="#df942a"
                  title="Web View"
                  onPress={() => this.props.navigation.navigate('WebView')}
              />
               <Text>  </Text>
              </ScrollView>
          </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
    title: {
      fontSize: 20,
      margin: 20,
      textAlign: 'center',
    },
  });

export default HomeScreen;
