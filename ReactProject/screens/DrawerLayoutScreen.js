import React, { Component } from 'react';
import { View, DrawerLayoutAndroid, Text, Button } from 'react-native';

export default class DrawerLayoutAndroidScreen extends Component {
    render() {
        var navigationView = (
            <View style={{ flex: 1, backgroundColor: '#fff' }}>
                <Text style={{ margin: 10, fontSize: 15, textAlign: 'left' }}>This is a Drawer Layout</Text>
            </View>
        );
        return (
            <DrawerLayoutAndroid
                drawerWidth={300}
                drawerPosition={DrawerLayoutAndroid.positions.Left}
                renderNavigationView={() => navigationView}>
                <View style={{ flex: 1, alignItems: 'center' }}>
                    <Text style={{ margin: 10, fontSize: 15, textAlign: 'right' }}>swipe right</Text>
                    <Text style={{ margin: 10, fontSize: 15, textAlign: 'right' }}>To view the drawer!</Text>
                    <Button color="#df942a"
                        title="Home"
                        onPress={() => this.props.navigation.navigate('Home')}
                    />
                </View>
            </DrawerLayoutAndroid>
        );
    }
}

