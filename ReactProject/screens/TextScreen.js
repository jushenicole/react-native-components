import React, { Component } from 'react';
import { Text, StyleSheet, Button, View } from 'react-native';

export default class TextScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      titleText: "Bird's Nest",
      bodyText: 'This is not really a bird nest.'
    };
  }

  render() {
    return (
      <View >
        <Text style={styles.titleText} onPress={this.onPressTitle}>
          {this.state.titleText}{'\n'}{'\n'}
        </Text>
        <Text numberOfLines={5}>
          {this.state.bodyText}
        </Text>
        <Button color="#df942a"
            title="Home"
            onPress={() => this.props.navigation.navigate('Home')}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  baseText: {
    fontFamily: 'Tahoma',
  },
  titleText: {
    fontSize: 20,
    fontWeight: 'bold',
  },
});
