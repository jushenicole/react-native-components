import React, { Component } from 'react';
import { ViewPagerAndroid, View, Text, StyleSheet, Button } from 'react-native';

export default class ViewPagerScreen extends Component {
    render() {
        return (
            <ViewPagerAndroid
                style={styles.viewPager}
                initialPage={0}>
                <View style={styles.pageStyle} key="1">
                    <Text>First page</Text>
                    <Text>Swipe left</Text>
                </View>
                <View style={styles.pageStyle} key="2">
                    <Text>Second page</Text>
                    <Text>Swipe right</Text>
                    <Button color="#df942a"
                        title="Home"
                        onPress={() => this.props.navigation.navigate('Home')}
                    />
                </View>
            </ViewPagerAndroid>
        )
    }
}

const styles = StyleSheet.create({
    viewPager: {
        flex: 1
    },
    pageStyle: {
        alignItems: 'center',
        padding: 20,
    }
});
